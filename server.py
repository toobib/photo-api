from fastapi import FastAPI
import uvicorn
from photo_api.routers import hello_world, public_picture, private_picture, admin

tags_metadata = [
    {
        "name": "API Status",
        "description": "provide the status of the API."
    },
    {
        "name": "Admin",
        "description": "Admin routes."
    },
    {
        "name": "Private Picture",
        "description": "handle pictures available only to the user."
    },
    {
        "name": "Public Picture",
        "description" : "handle pictures available to any."
    },
]

app = FastAPI(
    title="TOOBIB: Photo API",
    summary="API for handling users' pictures on the Toobib.org website.",
    description="""Write the necessary Markdown here""",
    contact={
        "name": "Toobib",
        "url": "https://www.xx.org",
        "email": "x@x.xx",
    },
    terms_of_service="https://www.test.de",
    version="0.0.1",
    openapi_tags=tags_metadata,
    license_info={
        "name": "Apache 2.0",
        "identifier": "MIT",
    },
)

app.include_router(hello_world.router)
app.include_router(private_picture.router)
app.include_router(public_picture.router)
app.include_router(admin.router)

if __name__ == "__main__":
    uvicorn.run("server:app", host="localhost", port=5000, reload=True)
