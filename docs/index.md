# Documentation

You are now reading the documentation for toobib-photoAPI ([toobib-photoAPI: Git repository](https://framagit.org/toobib/photo-api)).

## Introduction

This API goal is to deliver and update any pictures in use by the [toobib-website](https://toobib.org/) ([toobib-Website: Git repository](https://framagit.org/toobib/toobib-website). This API save and retrieve images uploaded by users or admins. We have categories of images that are divided in two main groups: 'public' and 'private'.

## API Specs

FastAPI automatically serve an OpenAPI documentation. Our is available here:
[Toobib searchAPI OpenAPI specs](https://search.dev.api.toobib.org/docs)

In the specs you will find a precise description of endpoints and the pydantic models.

## Project

```
├── photo_api
│   ├── routers          											# API endpoints
│   │   ├── admin.py													
│   │   ├── hello_world.py
│   │   ├── private_picture.py								# Endpoints for images only available to the user and the admin
│   │   ├── public_picture.py									# Endpoints for images available to anyone
│   │   └── __init__.py      									# needed for import
│   ├── helper           											# application utilities
│   │   ├── auth.py
│   │   ├── utils.py
│   │   └── __init__.py      									# needed for import
│   ├── config           											# application configuration
│   │   ├── user.py
│   │   └── __init__.py      									# needed for import
│   ├── tests                									# testing directory
│   │   ├── user.py
│   │   ├── conftest.py      									# pytest fixture
│   │   ├── test_auth.py
│   │   ├── test_pytest.py
│   │   ├── test_router_admin.py
│   │   ├── test_router_hello_world.py
│   │   ├── test_router_private_picture.py
│   │   ├── test_router_public_picture.py
│   │   ├── test_router_public_picture.py
│   │   └── __init__.py      									# needed for pytest to work
│   ├── server.py
│   └── __init__.py            								# needed for import
├── docs                 											# markdown project documentation
│   ├── index.py															# homepage
├── LICENSE
├── .gitignore
├── .gitlab-ci.yml       											# CI/CD configuration
├── Dockerfile
├── README.md
├── mkdocs.yml           											# documentation configuration
├── pyproject.toml       											# project metadata
└── requirements.txt     											# dependencies
```

## Installation

## Getting Started

This section will guide you through the setup process to get the project up and running on your local machine.

## Installation

### Virtual environment and dependencies

Before you start, make sure you have Python and pipx installed on your machine. Then, install the required libraries by running the following commands:

```bash
sudo apt-get install pipx
python3 -m venv .venv
source .venv/bin/activate
```

Install [poetry](https://python-poetry.org/docs/)

```
pipx install poetry
```

Install all the required python packages:

```
poetry install
deactivate
```


Deactivate the virtual env, and add the following line to the .venv/bin/activate file:

```bash
deactivate
export PYTHONPATH="/path/to/photo-api/"
```
### .env file Configuration

Before running the repository, create a .env file in '/photo_api' and add the following configurations. These are required for the API to communicate with Keycloak:

```
KEYCLOAK_HOST="https://keycloak-test.your-host.org"
REALM_NAME="realm_name"
CLIENT_ID="cliend_id"
CLIENT_SECRET="secret"
ALGORITHM="RS256"
AUDIENCE="account"
KEYCLOAK_OPENID_PATH="/realms/${REALM_NAME}/protocol/openid-connect"
KEYCLOAK_CERT_PATH="/certs"
KEYCLOAK_TOKEN_PATH="/token"
KEYCLOAK_BODY_ADMIN={"grant_type":"password", "client_id": "@{CLIENT_ID}", "username": "xxx", "password":"xx", "client_secret": "${CLIENT_SECRET}"}
KEYCLOAK_BODY_USER={"grant_type":"password", "client_id": "@{CLIENT_ID}", "username": "xxxx", "password":"xx", "client_secret": "${CLIENT_SECRET}"}
UNIQUE_ID="unique_id"
ADMIN="admin"
```

we install uvloop and httptools because they replace the [default asyncio event loop and HTTP parser and make the faster](https://github.com/Kludex/fastapi-tips).

## Running the Project

### In your terminal

```bash

source .venv/bin/activate // if not already activated
cd photo_api
python3 server.py
```

### In docker

first build the image

```commandline
sudo docker build -t photo-api .
```

then run it:

```commandline
sudo docker run -d --network=host --name photo-api-container -p 5001:5001 photo-api 
```

