from enum import Enum


class ModelPublicCategory(str, Enum):
    profile_picture = "profile"

class ModelPrivateCategory(str, Enum):
    cps_picture = "cps"
    profile_picture = "profile"
    miscellaneous = "misc"

class ModelResponseMode(str, Enum):
   pre_signed_url = "preSignedURL"
   bytesIO = "BytesIO"


