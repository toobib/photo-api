from pydantic_settings import BaseSettings, SettingsConfigDict

class Settings(BaseSettings):
    KEYCLOAK_HOST: str
    REALM_NAME: str
    CLIENT_ID: str
    ALGORITHM: str
    AUDIENCE: str
    KEYCLOAK_OPENID_PATH: str
    KEYCLOAK_CERT_PATH: str
    KEYCLOAK_TOKEN_PATH: str
    KEYCLOAK_BODY_ADMIN: str
    KEYCLOAK_BODY_USER: str
    UNIQUE_ID: str
    ADMIN: str
    CLIENT_SECRET: str
    S3_BUCKET_NAME: str
    S3_BUCKET_ENDPOINT: str
    S3_KEY: str
    S3_SECRET: str
    S3_REGION: str

    model_config = SettingsConfigDict(env_file='.env', env_file_encoding='utf-8')

settings = Settings()
