payload_admin = {
  "exp": 1726672425,
  "iat": 1726672125,
  "jti": "f61d8ba9-bef6-47cd-b977-9d084d080d8d",
  "iss": "https://keycloak-test.interhop.org/realms/Toobib",
  "aud": [
    "realm-management",
    "account"
  ],
  "sub": "e18d8f65-45ea-4c88-93f1-e157940f520b",
  "typ": "Bearer",
  "azp": "toobib",
  "session_state": "776d37f3-024a-44b5-a06d-05cd4985e962",
  "acr": "1",
  "allowed-origins": [
    "https://dev.front.toobib.org",
    "https://toobib.org",
    "http://localhost:3000"
  ],
  "realm_access": {
    "roles": [
      "access-toobib",
      "admin-private_picture"
    ]
  },
  "resource_access": {
    "realm-management": {
      "roles": [
        "view-identity-providers",
        "view-realm",
        "manage-identity-providers",
        "impersonation",
        "realm-admin",
        "create-client",
        "manage-users",
        "query-realms",
        "view-authorization",
        "query-clients",
        "query-users",
        "manage-events",
        "manage-realm",
        "view-events",
        "view-users",
        "view-clients",
        "manage-authorization",
        "manage-clients",
        "query-groups"
      ]
    },
    "toobib": {
      "roles": [
        "access"
      ]
    },
    "account": {
      "roles": [
        "manage-account",
        "manage-account-links"
      ]
    }
  },
  "scope": "email profile openid",
  "sid": "776d37f3-024a-44b5-a06d-05cd4985e962",
  "email_verified": False,
  "rpps": "0039100730",
  "name": "Adrien PARROT",
  "preferred_username": "adrien.parrot",
  "given_name": "Adrien",
  "family_name": "PARROT",
  "email": "adrien.parrot@caramail.fr"
}

payload_user = {
  "exp": 1726672555,
  "iat": 1726672255,
  "jti": "f01f5a51-b056-4862-8a85-7d1a9c14d2ea",
  "iss": "https://keycloak-test.interhop.org/realms/Toobib",
  "aud": "account",
  "sub": "acec4d53-2064-49d1-bb28-97b0bec32b17",
  "typ": "Bearer",
  "azp": "toobib",
  "session_state": "22493992-8974-4558-9ead-502276268d68",
  "acr": "1",
  "allowed-origins": [
    "https://dev.front.toobib.org",
    "https://toobib.org",
    "http://localhost:3000"
  ],
  "realm_access": {
    "roles": [
      "default-roles-toobib",
      "offline_access",
      "uma_authorization"
    ]
  },
  "resource_access": {
    "account": {
      "roles": [
        "manage-account",
        "manage-account-links",
        "view-profile"
      ]
    }
  },
  "scope": "email profile",
  "sid": "22493992-8974-4558-9ead-502276268d68",
  "email_verified": False,
  "name": "quentin parrot",
  "preferred_username": "parrot.quentin",
  "given_name": "quentin",
  "family_name": "parrot",
  "email": "parrot.quentin@gmail.com"
}

invalid_token = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJuaG5hS2JadVBETzRzY29GMkNTUWVFYS1WQ2NRbjFJbl83NnZIemtoQkc0In0.eyJleHAiOjE3MjYzOTUwMzksImlhdCI6MTcyNjM5NDczOSwianRpIjoiZmM1ODQ3OTEtMjgyMi00NjVmLWI1ZTAtMDM3ZTFlYTI1NDMyIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay10ZXN0LmludGVyaG9wLm9yZy9yZWFsbXMvVG9vYmliIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6ImUxOGQ4ZjY1LTQ1ZWEtNGM4OC05M2YxLWUxNTc5NDBmNTIwYiIsInR5cCI6IkJlYXJlciIsImF6cCI6InRvb2JpYiIsInNlc3Npb25fc3RhdGUiOiJmNDNjZTI4MS00Y2QxLTQ2ZDUtOWUxZC0zZTM2YzQ1YTVkN2IiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vZGV2LmZyb250LnRvb2JpYi5vcmciLCJodHRwczovL3Rvb2JpYi5vcmciLCJodHRwOi8vbG9jYWxob3N0OjMwMDAiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImFjY2Vzcy10b29iaWIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFsbS1tYW5hZ2VtZW50Ijp7InJvbGVzIjpbInZpZXctaWRlbnRpdHktcHJvdmlkZXJzIiwidmlldy1yZWFsbSIsIm1hbmFnZS1pZGVudGl0eS1wcm92aWRlcnMiLCJpbXBlcnNvbmF0aW9uIiwicmVhbG0tYWRtaW4iLCJjcmVhdGUtY2xpZW50IiwibWFuYWdlLXVzZXJzIiwicXVlcnktcmVhbG1zIiwidmlldy1hdXRob3JpemF0aW9uIiwicXVlcnktY2xpZW50cyIsInF1ZXJ5LXVzZXJzIiwibWFuYWdlLWV2ZW50cyIsIm1hbmFnZS1yZWFsbSIsInZpZXctZXZlbnRzIiwidmlldy11c2VycyIsInZpZXctY2xpZW50cyIsIm1hbmFnZS1hdXRob3JpemF0aW9uIiwibWFuYWdlLWNsaWVudHMiLCJxdWVyeS1ncm91cHMiXX0sInRvb2JpYiI6eyJyb2xlcyI6WyJhY2Nlc3MiXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSBvcGVuaWQiLCJzaWQiOiJmNDNjZTI4MS00Y2QxLTQ2ZDUtOWUxZC0zZTM2YzQ1YTVkN2IiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInJwcHMiOiIwMDM5MTAwNzMwIiwibmFtZSI6IkFkcmllbiBQQVJST1QiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZHJpZW4ucGFycm90IiwiZ2l2ZW5fbmFtZSI6IkFkcmllbiIsImZhbWlseV9uYW1lIjoiUEFSUk9UIiwiZW1haWwiOiJhZHJpZW4ucGFycm90QGNhcmFtYWlsLmZyIn0.03y9TegDv076b3bWsXTUQhNTsi8bFhDNndrwUueH3EW0nzO29z7Tu-Kr_2LDcv3YMsUSUeHNwnql-FzIjco3wl8i0zXqD9G7_0dtKsNLD5cVJNZO5vqBy3OWIQBLO2f02JGbza4iMAKKZgHMym0Rm84bhm-wGPMrzqALY8Kh97tcrHA7evu9bMN2CGhnl-FlxI_pwtgjpHZwb8BRJcb8uQBAKoRFQ_9nTdcre5xNAQ_BS5pL1bVeUeAbG_5oo77v3gWmg1ptEOpwVmW7CoNe2Vo21HoYHVyFokT4e7ux19iNdwuZo1PeEz4LBBgd-Gmu0QoH7qP6MYad9h031Qm6Zw"

expired_token = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJuaG5hS2JadVBETzRzY29GMkNTUWVFYS1WQ2NRbjFJbl83NnZIemtoQkc0In0.eyJleHAiOjE3MjYzOTUwMzksImlhdCI6MTcyNjM5NDczOSwianRpIjoiZmM1ODQ3OTEtMjgyMi00NjVmLWI1ZTAtMDM3ZTFlYTI1NDMyIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay10ZXN0LmludGVyaG9wLm9yZy9yZWFsbXMvVG9vYmliIiwiYXVkIjpbInJlYWxtLW1hbmFnZW1lbnQiLCJhY2NvdW50Il0sInN1YiI6ImUxOGQ4ZjY1LTQ1ZWEtNGM4OC05M2YxLWUxNTc5NDBmNTIwYiIsInR5cCI6IkJlYXJlciIsImF6cCI6InRvb2JpYiIsInNlc3Npb25fc3RhdGUiOiJmNDNjZTI4MS00Y2QxLTQ2ZDUtOWUxZC0zZTM2YzQ1YTVkN2IiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbImh0dHBzOi8vZGV2LmZyb250LnRvb2JpYi5vcmciLCJodHRwczovL3Rvb2JpYi5vcmciLCJodHRwOi8vbG9jYWxob3N0OjMwMDAiXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbImFjY2Vzcy10b29iaWIiXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFsbS1tYW5hZ2VtZW50Ijp7InJvbGVzIjpbInZpZXctaWRlbnRpdHktcHJvdmlkZXJzIiwidmlldy1yZWFsbSIsIm1hbmFnZS1pZGVudGl0eS1wcm92aWRlcnMiLCJpbXBlcnNvbmF0aW9uIiwicmVhbG0tYWRtaW4iLCJjcmVhdGUtY2xpZW50IiwibWFuYWdlLXVzZXJzIiwicXVlcnktcmVhbG1zIiwidmlldy1hdXRob3JpemF0aW9uIiwicXVlcnktY2xpZW50cyIsInF1ZXJ5LXVzZXJzIiwibWFuYWdlLWV2ZW50cyIsIm1hbmFnZS1yZWFsbSIsInZpZXctZXZlbnRzIiwidmlldy11c2VycyIsInZpZXctY2xpZW50cyIsIm1hbmFnZS1hdXRob3JpemF0aW9uIiwibWFuYWdlLWNsaWVudHMiLCJxdWVyeS1ncm91cHMiXX0sInRvb2JpYiI6eyJyb2xlcyI6WyJhY2Nlc3MiXX0sImFjY291bnQiOnsicm9sZXMiOlsibWFuYWdlLWFjY291bnQiLCJtYW5hZ2UtYWNjb3VudC1saW5rcyJdfX0sInNjb3BlIjoiZW1haWwgcHJvZmlsZSBvcGVuaWQiLCJzaWQiOiJmNDNjZTI4MS00Y2QxLTQ2ZDUtOWUxZC0zZTM2YzQ1YTVkN2IiLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInJwcHMiOiIwMDM5MTAwNzMwIiwibmFtZSI6IkFkcmllbiBQQVJST1QiLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhZHJpZW4ucGFycm90IiwiZ2l2ZW5fbmFtZSI6IkFkcmllbiIsImZhbWlseV9uYW1lIjoiUEFSUk9UIiwiZW1haWwiOiJhZHJpZW4ucGFycm90QGNhcmFtYWlsLmZyIn0.03y9TegDv076b3bWsXTUQhNTsi8bFhDNndrwUueH3EW0nzO29z7Tu-Kr_2LDcv3YMsUSUeHNwnql-FzIjco3wl8i0zXqD9G7_0dtKsNLD5cVJNZO5vqBy3OWIQBLO2f02JGbza4iMAKKZgHMym0Rm84bhm-wGPMrzqALY8Kh97tcrHA7evu9bMN2CGhnl-FlxI_pwtgjpHZwb8BRJcb8uQBAKoRFQ_9nTdcre5xNAQ_BS5pL1bVeUeAbG_5oo77v3gWmg1ptEOpwVmW7CoNe2Vo21HoYHVyFokT4e7ux19iNdwuZo1PeEz4LBBgd-Gmu0QoH7qP6MYad9h031QM6Zw'