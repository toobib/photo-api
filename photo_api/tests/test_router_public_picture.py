from starlette import status
from fastapi.testclient import TestClient

from photo_api.helper.auth import get_token
from photo_api.server import app
from photo_api.tests.users import expired_token

testClient = TestClient(app)


def test_get_public_picture_without_token():
    response = testClient.get(
        "/picture/public/profile/0039100730")
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["content-type"] == "application/octet-stream"
    assert response.content is not None


def test_get_public_picture_expired_token():
    response = testClient.get(
        "/picture/public/profile/0039100730",
        headers={ 'Authorization': f"Bearer {expired_token}"})
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["content-type"] == "application/octet-stream"
    assert response.content is not None


def test_get_public_picture_as_user():
    token = get_token("user").get("access_token")
    response = testClient.get(
        "/picture/public/profile/0039100730",
        headers={ 'Authorization': f"Bearer {token}"})
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["content-type"] == "application/octet-stream"
    assert response.content is not None


def test_get_public_picture_as_admin():
    token = get_token("admin").get("access_token")

    response = testClient.get(
        "/picture/public/profile/0039100730",
        headers={ 'Authorization': f"Bearer {token}"})
    print(response)
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["content-type"] == "application/octet-stream"
    assert response.content is not None