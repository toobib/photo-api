import os.path

import pytest
from starlette import status
from fastapi.testclient import TestClient

from photo_api.helper.auth import get_current_user
from photo_api.server import app
from photo_api.tests.users import payload_user, payload_admin

def override_get_current_user():
    return payload_user

def override_get_current_admin():
    return payload_admin

testClient = TestClient(app)


def test_admin_get_picture_as_user():
    app.dependency_overrides[get_current_user] = override_get_current_user
    response = testClient.get(
        "/admin/picture/cps/0039100730")
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json()['detail'] == "No access right"


testClientAdmin = TestClient(app)

def test_admin_get_picture_as_admin():
    app.dependency_overrides[get_current_user] = override_get_current_admin

    response = testClientAdmin.get(
        "/admin/picture/cps/0039100730")
    print(response)
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["content-type"] == "application/octet-stream"
    assert response.content is not None



"""
=========== POST REQUEST ===========
"""


def test_admin_post_picture_as_user():
    app.dependency_overrides[get_current_user] = override_get_current_user

    path = os.path.dirname(os.path.realpath(__file__))
    filename = "0039100730.png"
    with open(f"{path}/{filename}", 'rb') as file:
        response = testClient.post(
            "/admin/picture/cps/0039100730",
            files = {'file': (filename, file, "image/png")}
        )
    assert response.status_code == status.HTTP_401_UNAUTHORIZED
    assert response.json()['detail'] == "No access right"


def test_admin_post_picture_as_admin():
    app.dependency_overrides[get_current_user] = override_get_current_admin

    path = os.path.dirname(os.path.realpath(__file__))
    filename = "0039100730.png"
    with open(f"{path}/{filename}", 'rb') as file:
        response = testClient.post(
            "/admin/picture/cps/0039100730",
            files={'file': (filename, file, "image/png")}
        )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.headers["content-type"] == "application/json"
    assert response.content is not None


@pytest.fixture(autouse=True)
def clear_overrides():
    yield
    app.dependency_overrides = {}