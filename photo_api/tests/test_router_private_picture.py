import os

from starlette import status
from fastapi.testclient import TestClient

from photo_api.helper.auth import get_current_user
from photo_api.server import app
from photo_api.tests.test_router_admin import override_get_current_user, override_get_current_admin

testClient = TestClient(app)

def test_get_private_picture_as_user():
    app.dependency_overrides[get_current_user] = override_get_current_user
    response = testClient.get(
        "/picture/private/cps")
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json()['detail'] == 'Photo not found.'

def test_get_private_picture_as_admin():
    app.dependency_overrides[get_current_user] = override_get_current_admin
    response = testClient.get(
        "/picture/private/cps")
    print(response)
    assert response.status_code == status.HTTP_200_OK
    assert response.headers["content-type"] == "application/octet-stream"
    assert response.content is not None


"""
=========== POST REQUEST ===========
"""


def test_post_private_picture_wrong_category():
    app.dependency_overrides[get_current_user] = override_get_current_user
    path = os.path.dirname(os.path.realpath(__file__))
    filename = "0039100730.png"
    with open(f"{path}/0039100730.png", "rb") as file:
        # Send the POST request with the file
        response = testClient.post(
            "/picture/private_or_public/wrong_category",
            files={'file': (filename, file, "image/png")}  # Pass the file object here
        )
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
    assert response.json()['detail'][0].get('msg') ==  "Input should be 'cps', 'profile', 'misc' or 'test'"

def test_post_private_picture():
    app.dependency_overrides[get_current_user] = override_get_current_user
    path = os.path.dirname(os.path.realpath(__file__))
    filename = "0039100730.png"
    with open(f"{path}/{filename}", 'rb') as file:
        response = testClient.post(
            "/picture/private_or_public/profile",
            files={'file': (filename, file, "image/png")}
        )
    assert response.status_code == status.HTTP_201_CREATED
    assert response.headers["content-type"] == "application/json"
    assert response.content is not None







