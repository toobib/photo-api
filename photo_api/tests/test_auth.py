import pytest
from fastapi import HTTPException
from starlette import status

from photo_api.helper.auth import get_current_user
from photo_api.tests.users import expired_token, invalid_token


def test_get_current_user_expired_token():
    with pytest.raises(HTTPException) as exc_info:
        get_current_user(expired_token)  # Call the function that raises the exception

    assert exc_info.value.status_code == status.HTTP_401_UNAUTHORIZED
    assert exc_info.value.detail == "Token has expired"

def test_get_current_user_invalid_token():
    with pytest.raises(HTTPException) as exc_info:
        get_current_user(invalid_token)  # Call the function that raises the exception

    assert exc_info.value.status_code == status.HTTP_401_UNAUTHORIZED
    assert exc_info.value.detail == "Invalid token"
"""
No example of invalid_signature_token found
def test_get_current_user_invalid_signature():
    with pytest.raises(HTTPException) as exc_info:
        get_current_user(invalid_signature_token)

    assert exc_info.value.status_code == status.HTTP_401_UNAUTHORIZED
    assert exc_info.value.detail == "Signature verification failed"
"""