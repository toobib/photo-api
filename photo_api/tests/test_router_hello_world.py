def test_hello_world(test_client):
    response = test_client.get("/hello_world")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello world"}