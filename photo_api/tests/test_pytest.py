import pytest
from fastapi import HTTPException

from photo_api.helper.utils import secure_filename

pair_param_return_value_200 = [
    # 1. Valid with uppercase extension
    ("image.JPG", "image.jpg"),
    # 2. Valid with spaces in the filename
    ("my image file.jpeg", "my_image_file.jpeg"),
    # 3. Valid with special characters in the filename
    ("special!@#$%^&*()file.png", "special_file.png"),
    # 4. Valid with multiple dots in the filename
    ("complex.name.with.dots.jpg", "complex_name_with_dots.jpg"),
    # 7. Valid filename with extension but trailing spaces
    ("trailing_spaces .jpeg", "trailing_spaces.jpeg"),
    # 8. Valid filename with hyphens and underscores
    ("file-name_with-hyphens_and_underscores.png", "file_name_with_hyphens_and_underscores.png"),
    # 12. Valid filename with uppercase letters and extension
    ("IMAGE_WITH_UPPERCASE.JPEG", "IMAGE_WITH_UPPERCASE.jpeg"),
    # 11. File with leading dot (hidden file)
    (".hidden_file.png", "hidden_file.png"),
    ("my_file.PNG",    "my_file.png"),
    ("my_file.JPeg", "my_file.jpeg"),
    ("../../../etc/passwd.jpg", "etc_passwd.jpg"),
]

param_value_401 = [
    # 5. No extension
    "filename_without_extension",
    # 6. Empty string
    "",
    # 9. File with invalid extension
    "invalid_extension.txt",
    # 10. File with no filename (just extension)
    ".jpg",
    "my.file.name",
    "my_file",
]

@pytest.mark.parametrize("filename,expected_output", pair_param_return_value_200)
def test_secure_filename_valid_cases(filename, expected_output):
    assert secure_filename(filename) == expected_output


@pytest.mark.parametrize("filename", param_value_401)
def test_secure_filename_invalid_cases(filename):
    with pytest.raises(HTTPException) as exc_info:
        secure_filename(filename)
    assert exc_info.value.status_code == 401
    assert exc_info.value.detail == "Image must have a name and have a .png, .jpeg, .jpg extension"