import pytest
from fastapi.testclient import TestClient

from photo_api.server import app

@pytest.fixture(scope="function")
def test_client():
    """Create a test client that uses the override_get_db fixture to return a session.

    def override_get_db():
        try:
            yield db_session
        finally:
            db_session.close()

    app.dependency_overrides[get_db] = override_get_db"""
    with TestClient(app) as test_client:
        yield test_client