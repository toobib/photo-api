import re
from io import BytesIO
from fastapi import HTTPException
from PIL import Image
import os

def secure_filename(filename: str) -> str:
    # Strip leading/trailing whitespace and replace problematic characters
    match = re.match(r"(.+)\.(png|jpg|jpeg)$", filename, re.IGNORECASE)
    if match is not None:
        name, ext = match.groups()
        groups = re.findall(r"[a-zA-Z]+", name)
        name = "_".join(groups)
        # Replace space or . by "_"
        # name = re.sub(r"[ \.]", "_", name)
        # Remove any path separators or unsafe characters
        # name = re.sub(r'[^a-zA-Z0-9_-]', '', name)
        # erase all _ at start or at the end of the word
        # name = re.sub(r'^_*|_*$', '', name)
        filename = f"{name}.{ext.lower()}"
        return filename
    else:
        raise HTTPException(status_code=401, detail="Image must have a name and have a .png, .jpeg, .jpg extension")


def convert_picture_to_right_format(file : BytesIO, category: str = None) -> BytesIO:
    output_buffer = BytesIO()
    try:
        with Image.open(file) as im:
            if category == "profile":
                im = im.resize((256, 256))	
            im = im.convert("RGB")
            im.save(output_buffer, "JPEG", quality=100)
            output_buffer.seek(0)
        return output_buffer
    except OSError:
        print("cannot convert")
				
