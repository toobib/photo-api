import json
from typing import Annotated, Type

import jwt
import requests
from fastapi import Depends, HTTPException
from fastapi.security import OAuth2PasswordBearer

from photo_api.config.config import settings

import boto3

keycloak_host = settings.KEYCLOAK_HOST
keycloak_openid_path = settings.KEYCLOAK_OPENID_PATH
keycloak_token_path = settings.KEYCLOAK_TOKEN_PATH
keycloak_cert_path = settings.KEYCLOAK_CERT_PATH
keycloak_body_admin = settings.KEYCLOAK_BODY_ADMIN
keycloak_body_user = settings.KEYCLOAK_BODY_USER

s3_bucket_endpoint = settings.S3_BUCKET_ENDPOINT
s3_key = settings.S3_KEY
s3_secret = settings.S3_SECRET
s3_region = settings.S3_REGION

oauth2_bearer = OAuth2PasswordBearer(tokenUrl = keycloak_host + keycloak_openid_path + keycloak_token_path, )

def get_public_key(token):
    url = keycloak_host + keycloak_openid_path + keycloak_cert_path
    # PyJWKClient allows to get the right public key for a given token. public keys are rotating over time.
    # https://renzolucioni.com/verifying-jwts-with-jwks-and-pyjwt/
    jwks = jwt.PyJWKClient(url).get_signing_key_from_jwt(token).key
    return jwks

def get_token(user_type: str):
    url = keycloak_host + keycloak_openid_path + keycloak_token_path
    if user_type == "user":
        body = json.loads(keycloak_body_user)
    elif user_type == "admin":
        body = json.loads(keycloak_body_admin)
    else:
        raise { "message": "user_type is 'user' or 'admin'"}
    response = requests.post(url, data=body)
    return response.json()

def get_current_user(token: Annotated[str, Depends(oauth2_bearer)]):
    jwks = get_public_key(token)
    try:
        payload = jwt.decode(token, jwks, algorithms=[settings.ALGORITHM], audience=settings.AUDIENCE)
    except jwt.exceptions.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token has expired")
    except jwt.exceptions.InvalidTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")
    except jwt.exceptions.InvalidSignatureError:
        raise HTTPException(status_code=401, detail="Signature verification failed")
    return payload

user_dependency = Annotated[dict, Depends(get_current_user)]

def get_s3_client():
    s3 = boto3.client(
    's3',
    region_name='fr-par',
    endpoint_url=s3_bucket_endpoint,  # Replace with your endpoint
    aws_access_key_id=s3_key,
    aws_secret_access_key=s3_secret,
)
    return s3

s3_dependency = Annotated[boto3.session.Session, Depends(get_s3_client)]
