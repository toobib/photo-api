from fastapi import APIRouter
from starlette import status
from starlette.requests import Request

router = APIRouter(tags=["API Status"])

@router.get('/', status_code=status.HTTP_200_OK)
async def get_docs(request: Request) -> None:
    """
    Handles the root endpoint of the API.

    Returns:
        dict: A dictionary containing the message with the documentation URL.
    """
    return {"message": "Hello, explore our documentation at " + str(request.url) + "docs"}
