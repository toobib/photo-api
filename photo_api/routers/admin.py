import os.path

from io import BytesIO

from botocore.exceptions import ClientError, NoCredentialsError

from fastapi import APIRouter, HTTPException, UploadFile, File
from starlette import status
from starlette.responses import FileResponse, StreamingResponse

from photo_api.config.config import settings
from photo_api.helper.auth import user_dependency, s3_dependency
from photo_api.helper.utils import secure_filename, convert_picture_to_right_format
from photo_api.models import ModelPrivateCategory, ModelResponseMode

admin = settings.ADMIN
bucket_name = settings.S3_BUCKET_NAME


router = APIRouter(tags=["Admin"], prefix="/admin/picture")


@router.get("/{category}/{unique_id}", status_code=status.HTTP_200_OK)
async def get_image_as_admin(user: user_dependency, s3: s3_dependency, category: ModelPrivateCategory, unique_id: str, mode: ModelResponseMode):
    """
    Retrieves any image from any specified category. This endpoint is only accessible to admins.

    This endpoint allows administrators to retrieve an image either as a direct download or as a pre-signed URL, depending on the mode specified.
    
    Args:
        category (ModelPrivateCategory): The category of the image, used as part of the file path.
        unique_id (str): The unique identifier for the image. It will be the name of the image.
        mode (ModelResponseMode): The response mode; "preSignedUrl" returns a pre-signed URL, while any other mode streams the image content.
    
    Returns:
        StreamingResponse or str: The image content as a streaming response or a pre-signed URL.
    
    Raises:
        HTTPException:
            - 401 if the user lacks admin access rights.
            - 404 if the specified image is not found in S3.
            - 500 if there is an internal server error or S3 interaction fails.
    """
    roles = user.get('realm_access').get("roles")
    if admin not in roles:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="No access right")
    file_path = os.path.join(category, f"{unique_id}.jpg")
    try:
        if mode == "preSignedUrl":
            response = s3.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': file_path},
                                                    ExpiresIn=3600)
        else:	
            response = s3.get_object(Bucket=bucket_name, Key=file_path)
            image_content = response["Body"].read()
            response = StreamingResponse(
                BytesIO(image_content),
                media_type="image/jpeg"
            )
        return response
    except ClientError as e:
        if e.response["Error"]["Code"] == "NoSuchKey":
            raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="Image not found")
        else:
            raise HTTPException(status_code=HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to retrieve image")
       
@router.post("/{category}/{unique_id}", status_code=status.HTTP_201_CREATED)
async def post_image_as_admin(user: user_dependency, s3: s3_dependency, category: ModelPrivateCategory, unique_id: str, file : UploadFile = File(...)):
    """
    Uploads an image to the specified category. This endpoint access is restricted to administrators.
    
    This endpoint allows an admin user to upload an image to a specified category in the S3 bucket. 
    The image is automatically converted to the correct format before uploading.
    
    Args:
        category (ModelPrivateCategory): The category where the image will be stored.
        unique_id (str): A unique identifier for the image.
        file (UploadFile): The image file to be uploaded.
    
    Returns:
        dict: A success message with the filename of the uploaded image.
    
    Raises:
        HTTPException:
            - 401 if the user lacks admin access rights or S3 credentials are unavailable.
            - 400 if no file is provided.
            - 500 for any server-side error during upload or conversion.
    """

    roles = user.get('realm_access').get("roles")
    if admin not in roles:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail="No access right")
    if file.filename == '':
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='File is not given.')
    output_buffer = convert_picture_to_right_format(BytesIO(file.file.read()), category)
    filename=f"{unique_id}.jpg"
    try:
        s3.upload_fileobj(output_buffer, bucket_name, os.path.join(category, filename))
        return {'message': 'Photo uploaded successfully', 'filename': filename}
    except NoCredentialsError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="S3 Credentials not available")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
