import os

from io import BytesIO

from botocore.exceptions import NoCredentialsError, ClientError

from fastapi import APIRouter, HTTPException, UploadFile, File, Depends
from fastapi.responses import StreamingResponse
from starlette import status

from photo_api.config.config import settings
from photo_api.models import ModelPrivateCategory
from photo_api.helper.auth import user_dependency, s3_dependency
from photo_api.helper.utils import convert_picture_to_right_format

unique_id_string = settings.UNIQUE_ID
bucket_name = settings.S3_BUCKET_NAME


router = APIRouter(tags=["Private Picture"], prefix="/picture")


@router.get("/private/{category}", status_code=status.HTTP_200_OK)
async def get_private_image(user: user_dependency, s3: s3_dependency, category: ModelPrivateCategory, mode: str):
    """
    Retrieves a picture (accessible only to the user/owner of the image) from the S3 bucket, either as a pre-signed URL or as the actual image content.
    
    Args:
        category (ModelPublicCategory): The category of the image, used as part of the file path.
        mode (ModelResponseMode): The mode of response; "preSignedUrl" for a presigned URL or "stream" for image content.
    
    Returns:
        StreamingResponse or str: The image content as a streaming response or a presigned URL for temporary access.
    
    Raises:
        HTTPException: Raised with a 404 status if the image is not found, or 500 for other errors.
    """
    unique_id = user.get(unique_id_string)
    file_path = os.path.join(category, f"{unique_id}.jpg")
    try:
        if mode == "preSignedUrl":
            response = s3.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': file_path},
                                                    ExpiresIn=3600)
        else:	
            response = s3.get_object(Bucket=bucket_name, Key=file_path)
            image_content = response["Body"].read()
            response = StreamingResponse(
                BytesIO(image_content),
                media_type="image/jpeg"
            )
        return response
    except ClientError as e:
        # Handle errors (e.g., file not found)
        if e.response["Error"]["Code"] == "NoSuchKey":
            raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Image not found")
        else:
            raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to retrieve image")

@router.post("/private_or_public/{category}", status_code=status.HTTP_201_CREATED, tags=["Public Picture"])
async def post_image(user: user_dependency, s3: s3_dependency, category: ModelPrivateCategory, file : UploadFile = File(...)):
    """
    Uploads a picture (private or public) to the specified category in the S3 bucket. The filename is the unique_id of the user.
    
    The image is automatically converted to the correct format before uploading.

    Args:
        category (ModelPrivateCategory): The category where the picture will be stored (private or public).
        file (UploadFile): The file to be uploaded, expected to be a valid image.
    
    Returns:
        dict: A dictionary containing a success message and the filename of the uploaded picture.
    
    Raises:
        HTTPException: 
            - 400 if the file is not provided.
            - 401 if S3 credentials are unavailable.
            - 500 for any other server-side errors.
    """

    if file.filename == '':
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail='File is not given.')
    unique_id = user.get(unique_id_string)
    output_buffer = convert_picture_to_right_format(BytesIO(file.file.read()), category)
    filename=f"{unique_id}.jpg"
    try:
        s3.upload_fileobj(output_buffer, bucket_name, os.path.join(category, filename))
        return {'message': 'Photo uploaded successfully', 'filename': filename}
    except NoCredentialsError:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="S3 Credentials not available")
    except Exception as e:
        raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail=str(e))
