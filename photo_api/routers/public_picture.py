import os

from io import BytesIO 

from fastapi import APIRouter, Path, HTTPException
from fastapi.responses import FileResponse, StreamingResponse
from starlette import status

from botocore.exceptions import ClientError 

from photo_api.config.config import settings
from photo_api.models import ModelPublicCategory, ModelResponseMode
from photo_api.helper.auth import s3_dependency
from photo_api.config.config import settings

bucket_name = settings.S3_BUCKET_NAME


router = APIRouter(tags=["Public Picture"], prefix="/picture/public")


@router.get("/{category}/{unique_id}", status_code=status.HTTP_200_OK)
async def get_public_image(s3: s3_dependency, category: ModelPublicCategory, mode: ModelResponseMode, unique_id: str = Path(min_length=8)):
    """
    Retrieves a publicly available picture from the S3 bucket, either as a pre-signed URL or as the actual image content.
    
    Args:
        category (ModelPublicCategory): The category of the image, used as part of the file path.
        mode (ModelResponseMode): The mode of response; "preSignedUrl" for a presigned URL or "stream" for image content.
        unique_id (str): A unique identifier for the image, included in the file path.
    
    Returns:
        StreamingResponse or str: The image content as a streaming response or a presigned URL for temporary access.
    
    Raises:
        HTTPException: Raised with a 404 status if the image is not found, or 500 for other errors.
    """

    if unique_id is not None:
        file_path = os.path.join(category, f"{unique_id}.jpg")
        try:
            if mode == "preSignedUrl":
                response = s3.generate_presigned_url('get_object',
                                                        Params={'Bucket': bucket_name,
                                                                'Key': file_path},
                                                        ExpiresIn=3600)
            else:	
                response = s3.get_object(Bucket=bucket_name, Key=file_path)
                image_content = response["Body"].read()
                response = StreamingResponse(
                    BytesIO(image_content),
                    media_type="image/jpeg"
                )
            return response
        except ClientError as e:
            if e.response["Error"]["Code"] == "NoSuchKey":
                raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Image not found")
            else:
                raise HTTPException(status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="Failed to retrieve image")
    else:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="Image not found")
